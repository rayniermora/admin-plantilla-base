import { NgModule } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from "../shared/shared.module";
import { PAGES_ROUTES } from "./pages.routes";
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { FormsModule } from "@angular/forms";

import { DataTablesModule } from 'angular-datatables';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { PagesComponent } from "./pages.component";


@NgModule({
  declarations: [
    PagesComponent,
    DashboardComponent,        
  ],
  exports: [
    PagesComponent,
    DashboardComponent,    
  ],
  imports: [
    BrowserModule,
    SharedModule,
    PAGES_ROUTES,
    DataTablesModule,
    FormsModule,
    Ng4LoadingSpinnerModule.forRoot()
  ]
})

export class PagesModule {}
