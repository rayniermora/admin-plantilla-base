import { CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { APP_ROUTES } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { DataTablesModule } from 'angular-datatables';
import { PagenofoundComponent } from './pages/pagenofound/pagenofound.component';
import { SharedModule } from './shared/shared.module';
import { PagesModule } from './pages/pages.module';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PagenofoundComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    PagesModule,
    SharedModule,
    APP_ROUTES,
    HttpClientModule,
    DataTablesModule,
    Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
