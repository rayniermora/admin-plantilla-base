import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PagesComponent } from './pages/pages.component';
import { PagenofoundComponent } from './pages/pagenofound/pagenofound.component';


const routes: Routes = [

  {path: '', component: LoginComponent},
  { path: '',
    component: PagesComponent,
    loadChildren: './pages/pages.module#PagesModule'
  },
  { path: '**', component: PagenofoundComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' }

];


export const APP_ROUTES = RouterModule.forRoot(routes, {useHash: true});

