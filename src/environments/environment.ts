// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  // url: 'http://127.0.0.1:8000/api/',
  // link: 'http://127.0.0.1:8000/img/'
  url: 'http://ec2-3-94-43-198.compute-1.amazonaws.com/api/',
  link: 'http://ec2-3-94-43-198.compute-1.amazonaws.com/img/'
};
